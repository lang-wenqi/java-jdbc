package com.lang.dao.impl;

import com.lang.dao.StudentDao;
import com.lang.domain.Student;
import com.lang.util.DataSourceUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    @Override
    public List<Student> findAll() {
        ArrayList<Student> stus = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = DataSourceUtils.getConnection();
            ps = conn.prepareStatement("select * from student");
            rs = ps.executeQuery();
            while (rs.next()) {
                int sid = rs.getInt("sid");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                Date birthday = rs.getDate("birthday");

                Student stu = new Student(sid, name, age, birthday);
                stus.add(stu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.close(conn, ps, rs);
        }
        return stus;
    }

    @Override
    public void addStu(Student stu) {
        Connection conn = null;
        PreparedStatement ps = null;


        try {
            conn = DataSourceUtils.getConnection();
            ps = conn.prepareStatement("insert into student values(null,?,?,?)");

            ps.setString(1,stu.getName());
            ps.setInt(2,stu.getAge());
            java.util.Date date = stu.getBirthday();
            String birthday = new SimpleDateFormat("yyyy-MM-dd").format(date);
            ps.setString(3, birthday);

            ps.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.close(conn, ps, null);
        }
    }

    @Override
    public void deleteStu(Integer integer) {
        Connection conn = null;
        PreparedStatement ps = null;


        try {
            conn = DataSourceUtils.getConnection();
            ps = conn.prepareStatement("delete from student where sid = ?");
            ps.setInt(1, integer);
            ps.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.close(conn, ps, null);
        }
    }

    @Override
    public Student findBySid(Integer integer) {
        Student stu =null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = DataSourceUtils.getConnection();
            ps = conn.prepareStatement("select * from student where sid = ?");

            ps.setInt(1, integer);

            rs = ps.executeQuery();

            if (rs.next()) {
                int sid = rs.getInt("sid");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                Date birthday = rs.getDate("birthday");
                stu = new Student(sid, name, age, birthday);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.close(conn, ps, rs);
        }
        return stu;
    }

    @Override
    public void updateStu(Student stu) {
        Connection conn = null;
        PreparedStatement ps = null;


        try {
            conn = DataSourceUtils.getConnection();
            ps = conn.prepareStatement("update student set name = ?,age = ?,birthday = ? where sid =?");
            ps.setString(1, stu.getName());
            ps.setInt(2, stu.getAge());
            java.util.Date birthday = stu.getBirthday();
            String birStr = new SimpleDateFormat("yyyy-MM-dd").format(birthday);
            ps.setString(3, birStr);
            ps.setInt(4, stu.getSid());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.close(conn, ps, null);
        }
    }
}
