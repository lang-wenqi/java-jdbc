package com.lang.dao;

import com.lang.domain.Student;

import java.util.List;

public interface StudentDao {
    // 查询所有学生
    List<Student> findAll();

    void addStu(Student stu);

    void deleteStu(Integer integer);

    Student findBySid(Integer integer);

    void updateStu(Student stu);
}
