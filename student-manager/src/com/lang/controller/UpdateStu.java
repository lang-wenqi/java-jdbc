package com.lang.controller;

import com.lang.domain.Student;
import com.lang.service.StudentService;
import com.lang.service.impl.StudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/updateStu")
public class UpdateStu extends HttpServlet {
    private StudentService studentService = new StudentServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");

        String sid = req.getParameter("sid");
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        String birthday = req.getParameter("birthday");

        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Student stu = new Student(Integer.parseInt(sid), name, Integer.parseInt(age), date);

        studentService.updateStu(stu);

        resp.sendRedirect(req.getContextPath()+"/findAll");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
