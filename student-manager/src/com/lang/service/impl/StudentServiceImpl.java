package com.lang.service.impl;

import com.lang.dao.StudentDao;
import com.lang.dao.impl.StudentDaoImpl;
import com.lang.domain.Student;
import com.lang.service.StudentService;

import java.util.List;

public class StudentServiceImpl implements StudentService {
    private StudentDao studentDao = new StudentDaoImpl();

    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    @Override
    public void addStu(Student stu) {
        studentDao.addStu(stu);
    }

    @Override
    public void deleteStu(Integer integer) {
        studentDao.deleteStu(integer);
    }

    @Override
    public Student findBySid(Integer integer) {
        return studentDao.findBySid(integer);
    }

    @Override
    public void updateStu(Student stu) {
        studentDao.updateStu(stu);
    }
}
