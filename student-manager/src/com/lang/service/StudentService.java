package com.lang.service;

import com.lang.domain.Student;

import java.util.List;

public interface StudentService {
    // 查询所有学生
    List<Student> findAll();

    // 添加学生
    void addStu(Student stu);

    // 删除学生
    void deleteStu(Integer integer);

    // 根据sid查询学生
    Student findBySid(Integer integer);

    // 修改学生
    void updateStu(Student stu);
}
