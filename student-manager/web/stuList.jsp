<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>学生表</title>
    <style>
        tr{
            text-align: center;
        }
    </style>
</head>
<body>
    <table align="center" width="40%" border="1px">
        <tr>
            <th>学号</th>
            <th>姓名</th>
            <th>年龄</th>
            <th>生日</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${stus}" var="stu">
            <tr>
                <td>${stu.sid}</td>
                <td>${stu.name}</td>
                <td>${stu.age}</td>
                <td>${stu.birthday}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/forwardUpdate?sid=${stu.sid}">修改</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="${pageContext.request.contextPath}/deleteStu?sid=${stu.sid}">删除</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <div style="text-align: right"><a href="${pageContext.request.contextPath}/addStu.jsp">添加学生</a></div>
</body>
</html>
