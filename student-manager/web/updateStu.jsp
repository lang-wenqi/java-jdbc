<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生修改</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/updateStu" method="post">
    <input type="hidden" name="sid" value="${stu.sid}"/>
    <table align="center" width="40%">
        <tr>
            <td>
                <label for="name">姓名</label>
            </td>
            <td>
                <input type="text" name="name" id="name" value="${stu.name}" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="age">年龄</label>
            </td>
            <td>
                <input type="text" name="age" id="age" value="${stu.age}"/>
            </td>
        </tr>
        <tr>
            <td>
                <label for="birthday">生日</label>
            </td>
            <td>
                <input type="date" name="birthday" id="birthday" value="${stu.birthday}"/>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <input type="submit" value="修改" />
            </td>
        </tr>
    </table>
</form>
</body>
</html>
