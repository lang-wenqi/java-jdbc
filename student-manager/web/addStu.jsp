<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生添加</title>
    <style>
        tr{
            text-align: center;
        }
    </style>
</head>
<body>
    <form action="${pageContext.request.contextPath}/addStu" method="post">
        <table align="center" width="40%">
            <tr>
               <td>
                   <label for="name">姓名</label>
               </td>
                <td>
                    <input type="text" name="name" id="name" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="age">年龄</label>
                </td>
                <td>
                    <input type="text" name="age" id="age"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="birthday">生日</label>
                </td>
                <td>
                    <input type="date" name="birthday" id="birthday"/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <input type="submit" value="添加" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
