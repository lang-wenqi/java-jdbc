package com.lang.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();

        Map mapProxy = (Map) Proxy.newProxyInstance(
                map.getClass().getClassLoader(),
                map.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (method.getName().equals("put")) {
                            String key = (String) args[0];
                            String value = (String) args[1];
                            args[0] = key.toUpperCase();
                            args[1] = value.toUpperCase();
                            return method.invoke(map, args);
                        } else {
                            return method.invoke(map, args);
                        }
                    }
                }
        );

        mapProxy.put("abc", "bcd");
        System.out.println(mapProxy.get("ABC"));
        System.out.println(mapProxy);

    }
}
